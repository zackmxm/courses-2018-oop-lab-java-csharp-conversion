package it.unibo.oop.util;

public interface Tuple {
    int getSize();
    Object get(int i);
    Object[] toArray();

    static <A> Tuple1<A> of(A value) {
        return new Tuple1Impl<>(value);
    }

    static <A, B> Tuple2<A, B> of(A value1, B value2) {
        return new Tuple2Impl<>(value1, value2);
    }

    static <A, B, C> Tuple3<A, B, C> of(A value1, B value2, C value3) {
        return new Tuple3Impl<>(value1, value2, value3);
    }
}
